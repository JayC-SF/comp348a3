# Comp348a3
## Environment setup
Since the development was performed on a Macbook computer, the environment setup from the assignment did not work. However, a small edit of the configuration was done. The class path was added through the alias below instead:
```bash
alias clj="clj -Scp $(clj -Spath):./ -M"
```
The command above is similar to editing the classpath as requested in the assignment. In fact, it simply appends the proper classpath every time the `clj` command is used. 

**Note that if the environment setup suggested by the assignment was performed correctly, it is NOT necessary to use the command above.**

## Notes

Upon displaying the student record command, the courses are ordered in alphabetical letters and numerical order. The order is in descending order from the top to bottom. This is following the order provided in the example of the assignment. Since no order was specified for the course name, then the descending order was used.