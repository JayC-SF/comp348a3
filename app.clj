(ns app
  (:require [db])
  (:require [menu])
  (:require [clojure.string :as str]))

;; ------------------------ DECLARATION SECTION ------------------------
(declare commands)
(declare str_commands)
(declare gpa_scheme)
(declare studDB)
(declare courseDB)
(declare gradeDB)
;; ------------------------ FUNCTION SECTION ------------------------

(defn find_first
  "This function performs a find first, and returns nil if not found."
  [callback collection]
  (if (empty? collection)
    nil
    (if (callback (first collection))
      (first collection)
      (recur callback (rest collection)))))

(defn print_records 
  "This function prints records with their quotation mark"
  [records]
  ;; map every records to str so they can be pretty printed
  ;; This makes sure that every record will printed with their quotation marks.
  ;; use print_many function to prints each record in a new line.
  (menu/print_many (map #(% :str) records)))

;; This function uses functional programming to create an "object" of type command
;; It returns the proper attributes of a command upon calling it.
(defn create_command 
  "
  This function creates a function which holds information of a command.
  Upon calling the resulting function using the keywords :get_cmd, :get_str, :get_func
  it will return the values initialized with.
  "
  [command command_str func]
  ;; return a function that returns the attributes
  (fn [attribute] 
    (case attribute
      :get_cmd command
      :get_str command_str
      :get_func func)))

(defn find_command 
  "This function finds the command associated to the given input.
   It loops through the command list and finds the right command"
  [input]
  (let [in_lower (str/lower-case input)]
    (find_first #(= in_lower (str/lower-case (% :get_str))) commands)))

(defn get_grade_record 
  "This function finds the grade record of a specific student id"
  [sid]
  (let [stud_grades (filter #(= (% :sid) sid) gradeDB)]
    (when (seq stud_grades)
       (map
         ;; anonymous function
         (fn [grade]
           (let [course (find_first #(= (% :cid) (grade :cid)) courseDB)]
             (if (db/!= course nil)
               [(str (course :cournam) " " (course :courno)) (course :desc) (grade :semester) (grade :grade)]
               ["unkown" "unkown" (grade :semester) (grade :grade)])))
         stud_grades))))

;; Upon displaying the student record command, the courses are ordered in alphabetical letters 
;; and numerical order. The order is in descending order from the top to bottom. 
;; This is following the order provided in the example of the assignment. 
;; Since no order was specified for the course name, then the descending order was used.
(defn student_record 
  "This function requests an id from the student record and prints the student record."
  []
  ;; bind id to the next user input.
  (let [sid (menu/get_next_input "Enter student id: ")
        student (find_first #(= sid (% :sid)) studDB)]
    (if (= student nil)
      ;; condition if student is nil
      (println "Invalid student id:" (str "`" sid "`"))
      ;; condition if student not nil
      (do
        ;; print the first two fields of the student record
        (println (str [(student :sid) (student :name)]))
        (let [grade_record (get_grade_record sid)]
          (if (seq grade_record)
            ;; sort and print records
            (menu/foreach println (map str (sort #(compare (first %2) (first %1)) grade_record )))
            ;; grade_record is empty so no record found
            (println "No records for student:" sid)))))))

(defn calculate_gpa 
  "This function calculates the gpa of a student given a collection of grades"
  [stud_grades]
  (let [ stud_course_credits
    (map
     (fn [grade]
       (let [course (find_first #(= (% :cid) (grade :cid)) courseDB)]
         (if (db/!= course nil)
           (* (Integer/parseInt (course :credits)))
           0)))
     stud_grades)]
    (/ 
     (reduce + 
       (menu/combine #(* %1 (get gpa_scheme (%2 :grade))) stud_course_credits stud_grades))
     (reduce + stud_course_credits))))

(defn calculate_student_gpa 
  "Command function to calcualate gpa of a student"
  []
  (let [sid (menu/get_next_input "Enter student id: ")
        student (find_first #(= sid (% :sid)) studDB)]
    (if (= student nil)
       ;; condition if student is nil
      (println "Invalid student id:" (str "`" sid "`"))
      (let [stud_grades (filter #(= (% :sid) sid) gradeDB)]
        (if (seq stud_grades)
          (println (Double/parseDouble (format "%.2f" (calculate_gpa stud_grades))))
          (println "No records found for student:" sid))))))

(defn calculate_course_avg 
  "This function calculates the course average given a course input"
  [course]
  (let [semester_grades (group-by #(% :semester) (filter #(= (% :cid) (course :cid)) gradeDB))]
    (map
     (fn [semester]
       (let [grades (map #(get gpa_scheme (% :grade)) (get semester_grades semester))]
          [(str (course :cournam) " " (course :courno)) semester (/ (reduce + grades) (count grades))]))
     (keys semester_grades))))

(defn course_avg 
  "Command function to calcualte the course averages given a user input"
  []
  (let [course_input (menu/get_next_input "Enter the course name (e.g `COMP 348`): ")
        course_str (str/lower-case course_input)
        course (find_first #(= course_str (str/lower-case (str (% :cournam) " " (% :courno)))) courseDB)]
    (if (db/!= nil course_input)
      (let [avg (calculate_course_avg course)]
        (if (seq avg)
          (menu/print_many (map str avg))
          (println "No grade submitted yet for this course.")))
      
      (println "No records found for course: " course))))

(defn -main
  "This function starts the program"
  [continue]
  (if continue
    (let [
          input (menu/get_next_input "Enter your option here: ")
          cmd_func (find_command input)
          ]
        (if (= nil cmd_func)
          (do 
            (when (db/!= input "") 
              (println "Invalid input:" input)
            )
            (recur true))
          (if (= :exit (cmd_func :get_cmd))
            (recur false)
            (do ((cmd_func :get_func)) (recur true)))))
    (println "Goodbye!")))
;; ------------------------ DATA SECTION -----------------------

;; Map each of the data to function objects. This way it is easier to access specific fields.
;; <studID, name, address, phoneNumber> and map to student functions
(def studDB (map db/student (db/loadData "studs.txt")))
;; <id, cournam, courno, credits, desc> and map to course functions 
(def courseDB (map db/course (db/loadData "courses.txt")))
;; <studID, courseID, semester, grade> and map to grade functions
(def gradeDB (map db/grade (db/loadData "grades.txt")))

;; commands of the menu
;; Use the create_command to return a function specific to a particular command.
(def commands [
    (create_command :display_courses "Display Courses" #(print_records courseDB))
    (create_command :display_students "Display Students" #(print_records studDB))
    (create_command :display_grades "Display Grades" #(print_records gradeDB))
    (create_command :display_student_record "Display Student Record" student_record)
    (create_command :calculate_gpa "Calculate GPA" calculate_student_gpa)
    (create_command :course_avg "Course Average" course_avg)
    (create_command :print_menu "Print Menu" #(menu/print_menu str_commands))
    (create_command :exit "Exit" nil)
])

(def str_commands (map #(% :get_str) commands))

(def gpa_scheme { "A+" 4.3, "A" 4, "A-" 3.7, "B+" 3.3, "B" 3, "B-" 2.7, "C+" 2.3, "C" 2, "C-" 1.7, "D+" 1.3, "D" 1, "D-" 0.7, "F" 0})

;; ------------------------ START SECTION -----------------------

;; print the menu to start, note that each element is a function, therefore we call
;; each element to provide their string command.
(menu/print_menu str_commands)
(-main true)