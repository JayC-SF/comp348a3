(ns db
    (:require [clojure.string :as str])
)

(defn- get_record_field 
  "This function gets the appropriate field or a record using the index of the fields"
  [fields record field]
  (get record (.indexOf fields field)))

(defn- record_obj
   "This function is a typical record object, which given fields and a record it returns the
   the appropriate field for a record."
  [fields record keyword]
  (let [res (get_record_field fields record keyword)]
    (if (= res nil)
      (case keyword
        :str (str record)
        :record record)
      res)))

(defn student
  "This function returns a function representing a student"
  [record]
  ;; use partial to return a function with 1 arity
  (partial record_obj [:sid :name :address :phone] record))

(defn course 
  "This function returns a function representing a course"
  [record]
  ;; use partial to return a function with 1 arity
  (partial record_obj [:cid :cournam :courno :credits :desc] record))

(defn grade 
  "This function returns a function representing a grade"
  [record]
  ;; use partial to return a function with 1 arity
  (partial record_obj [:sid :cid :semester :grade] record))

(defn !=
  "This is used to define a not equal function"
  [& args]
  (not (apply = args)))

(defn loadData
    "
    This function reads a file to be split and cleaned for parsing.
    The resulting data structure should be a collection of records.
    "
    [filename]
    (into []
        ;; map each trimmed line to a vector containing elements of the records
        (map 
            ;; anonymous function argument for the map functionthat takes in a line 
            ;; and returns a vector of cleaned fields separated by |.
            #(into [] (map str/trim (str/split % #"\|")))
            ;; filter function that removes any empty strings from the collection
            ;; trim the lines using map
            ;; slurp the content of the file and split 
            (filter #(!= % "") (map str/trim (str/split (slurp filename) #"\n")))
        )
    ))
