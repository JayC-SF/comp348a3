(ns menu
    (:require [clojure.string :as str]))

;; --------------------------- FUNCTION SECTION OF CODE ---------------------------
;; for each command for utility
(defn foreach
  "This function is similar to functional for each functions. It performs a set of specific 
  action by applying a callback function to each element
  It always returns nil"
  [callback collection]
  ;; Base condition, if l is not empty, apply call back to the first element
  ;; Then recursive call with one less item.
  (when (seq collection)
    (callback (first collection)) (recur callback (rest collection))))

(defn print_many
  "This function prints the list given adding a \n to each element"
  [l]
  ;; Use foreach function, and pass println as a callback function
  (foreach println l))

(defn combine 
  "This function returns a list of elements where the two lists are mapped 
  into a result list. The between the two elements is performed with the callback function.
  The function combines up to the smallest list, therefore, the result list will be 
  as long as the smallest collection of the two."
  [callback list1 list2]
  ((fn
    ;; anonymous function that performs the same thing, but uses an extra parameter 
    ;; for continuation.
    [l1 l2 result]
    (if (and (seq l1) (seq l2))
        ;; if true
        ;; Tail recursively call the next iteration by removing first element
        ;; and using the callback to be added into the vector.
        (recur (rest l1) (rest l2) (conj result (callback (first l1) (first l2))))
        ;; if false
        result))
    ;; arguments of anonymous function
    list1 list2 []))

(defn get_next_input
  "This functions fetches the next command and lowercases the command"
  [msg]
  (print msg)
  (flush)
  (str/trim (read-line)))

(defn create_menu_list
  "This function takes in a list of elements, and sets a string prefix of their respective numbers
  For example, given ['foo' 'bar'] it will return ['1. foo' '2. bar']"
  [str_commands]
  ;; get the list of numbers 1, 2, 3, 4, 5, .... (# of elements in `str_commands`)
  (let [numbered_list (range 1 (+ 1 (count str_commands)))]
    ;; merge the numbers with the str_commands, this way it will create a list of str_commands
    ;; where the numbers are prefix to it.
    (combine #(str %1 ". " %2) numbered_list str_commands)))

(defn print_menu 
  "This function prints a menu given a list of string commands"
  [str_commands]
  ;; Get the menu, and combine it with the range of count
  ;; Combining the two lists will result in "1. Display courses" "..." "..."
  ;; Where the numbers are combined with the list
  (println "*** SIS Menu ***")
  (println "------------------")
  (print_many (create_menu_list str_commands))
  (println "\nFor example, you may enter `Display student record`.\n"))